package com.gozon.myttc.viewmodel.finchstation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.gozon.myttc.dto.FinchStationRoutesDto
import com.gozon.myttc.repository.api.ApiService
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class FinchStationViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val mainScheduler = TestScheduler()


    @Test
    fun `successful getJson`() {
        val sampleResponse =
            FinchStationRoutesDto(uri = "uri", name = "name", time = 123, stops = null)

        //given
        val apiService = mockk<ApiService> {
            every { getJsonFile() }
                .returns(Observable.just(sampleResponse))
        }

        val viewModel = FinchStationViewModel(
            apiService,
            mainScheduler
        )

        //when
        viewModel.getDataFromServer()
        Assert.assertEquals(viewModel.loader.value, true)
        mainScheduler.triggerActions()

        //then

        //Added thread because livedata.value is null without the delay
        Thread {
            Thread.sleep(500)
            viewModel.finchStationData.value.shouldBeInstanceOf<Success>()
            Assert.assertEquals(viewModel.loader.value, false)
        }
    }

    @Test
    fun `getting JSON With IOException must return NetworkError()`() {

        val throwable = IOException()


        val apiService = mockk<ApiService> {
            every { getJsonFile() }
                .returns(Observable.error(throwable))
        }

        val viewModel = FinchStationViewModel(
            apiService,
            mainScheduler
        )

        viewModel.getDataFromServer()
        Assert.assertEquals(viewModel.loader.value, true)
        mainScheduler.triggerActions()

        //Added thread because livedata.value is null without the delay
        Thread {
            Thread.sleep(500)
            viewModel.finchStationData.value.shouldBeInstanceOf<IOException>()
            Assert.assertEquals(viewModel.loader.value, false)
        }
    }


    @Test
    fun `getting JSON with other exception must return UnknownError()`() {

        val throwable = Throwable("hahaha")

        val apiService = mockk<ApiService> {
            every { getJsonFile() }
                .returns(Observable.error(throwable))
        }

        val viewModel = FinchStationViewModel(
            apiService,
            mainScheduler
        )

        viewModel.getDataFromServer()
        Assert.assertEquals(viewModel.loader.value, true)
        mainScheduler.triggerActions()

        //Added thread because livedata.value is null without the delay
        Thread {
            Thread.sleep(500)
            viewModel.finchStationData.value.shouldBeInstanceOf<UnknownError>()
            Assert.assertEquals(viewModel.loader.value, false)
        }


    }


}