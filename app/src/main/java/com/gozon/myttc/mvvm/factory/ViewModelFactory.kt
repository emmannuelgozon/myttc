package com.gozon.myttc.mvvm.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gozon.myttc.repository.api.ApiService
import com.gozon.myttc.viewmodel.finchstation.FinchStationViewModel
import io.reactivex.Scheduler
import java.security.InvalidParameterException


class ViewModelFactory(
    private val apiService: ApiService,
    private val mainScheduler: Scheduler
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        //you can add more viewmodels here
        when (modelClass) {
            FinchStationViewModel::class.java -> {
                return FinchStationViewModel(apiService, mainScheduler) as T
            }
            else -> throw InvalidParameterException(modelClass.simpleName)
        }
    }
}