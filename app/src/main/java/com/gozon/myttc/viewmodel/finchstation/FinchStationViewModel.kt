package com.gozon.myttc.viewmodel.finchstation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gozon.myttc.dto.FinchStationRoutesDto
import com.gozon.myttc.repository.api.ApiService
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException

class FinchStationViewModel(
    private val apiService: ApiService,
    private val mainScheduler: Scheduler
) : ViewModel() {

    val finchStationData = MutableLiveData<JSONResult>()

    val loader = MutableLiveData<Boolean>()

    private var disposable = Disposables.disposed()

    override fun onCleared() = disposable.dispose()

    fun getDataFromServer() {
        disposable = apiService.getJsonFile()
            .subscribeOn(Schedulers.io())
            .observeOn(mainScheduler)
            .doOnSubscribe { loader.value = true }
            .doOnTerminate { loader.value = false }
            .subscribe(
                {
                    finchStationData.value = Success(it)
                }, ::onFailed
            )
    }


    private fun onFailed(throwable: Throwable) {
        finchStationData.value = when (throwable) {
            is IOException -> {
                NetworkError()
            }
            else -> {
                UnknownError(throwable.message)
            }
        }
    }
}

interface JSONResult

class Success(val response: FinchStationRoutesDto) : JSONResult
class NetworkError : JSONResult
class UnknownError(val message: String?) : JSONResult