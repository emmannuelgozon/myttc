package com.gozon.myttc.repository.api

import com.gozon.myttc.dto.FinchStationRoutesDto
import io.reactivex.Observable

class ApiServiceImpl : ApiService {

    private val apiService: ApiService = HttpClient().getClient()!!.create(ApiService::class.java)

    override fun getJsonFile(): Observable<FinchStationRoutesDto> =
        apiService.getJsonFile()

}