package com.gozon.myttc.repository.api

import com.gozon.myttc.dto.FinchStationRoutesDto
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {

    @GET("finch_station.json")
    fun getJsonFile(): Observable<FinchStationRoutesDto>
}