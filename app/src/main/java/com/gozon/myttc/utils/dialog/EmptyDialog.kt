package com.gozon.myttc.utils.dialog

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.gozon.myttc.R
import com.gozon.myttc.databinding.DialogEmptyDataBinding

class EmptyDialog() : DialogFragment(R.layout.dialog_empty_data) {


    constructor(action: EmptyResources) : this() {
        this.action = action
    }


    private var dialogEmptyDataBinding: DialogEmptyDataBinding? = null

    private lateinit var binding: DialogEmptyDataBinding

    private var action: EmptyResources? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = DialogEmptyDataBinding.bind(view)
        dialogEmptyDataBinding = binding

        isCancelable = false

        val window = dialog!!.window!!
        val layoutParams: WindowManager.LayoutParams = window.attributes
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.CENTER
        window.attributes = layoutParams


        binding.emptyDialogTitle.text = action?.title
        binding.emptyDialogDescription.text = action?.description
        binding.emptyDialogCloseBtn.setOnClickListener {
            dismiss()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        dialogEmptyDataBinding = null
    }
}

enum class EmptyResources(val title: String, val description: String) {
    EMPTY_ROUTES("No stops available", "Stops are not available at this time."),
    EMPTY_STOP_TIMES("empty stop times", "list for stop times is empty")
}