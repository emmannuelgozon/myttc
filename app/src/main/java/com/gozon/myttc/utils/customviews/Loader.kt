package com.gozon.myttc.utils.customviews

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.gozon.myttc.R

class Loader : ConstraintLayout {

    constructor(context: Context) : super(context) { init() }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) { init() }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) { init () }

    private fun init() {
        inflate(context, R.layout.loader_layout, this)
        Glide.with(this).load(R.drawable.loading_gif).into(findViewById(R.id.loaderImageView))
    }
}