package com.gozon.myttc.utils.extensions

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Long.toReadableTime(format: String): String {
    val date = Date(this * 1000)
    val format = SimpleDateFormat(format, Locale.ENGLISH)
    return format.format(date)
}