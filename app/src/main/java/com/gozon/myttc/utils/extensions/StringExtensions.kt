package com.gozon.myttc.utils.extensions

fun String.addTimePeriod() : String{

    val replaceWith = when(this.drop(length - 1)) {
        "a" -> " AM"
        "p" -> " PM"
        else ->  ""
    }

    return this.dropLast(1)  + replaceWith

}