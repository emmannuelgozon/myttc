package com.gozon.myttc.ui.main

import com.gozon.myttc.dto.FinchStationRoutesDto

interface MainInterface {

    fun getFinchStationRoutes(): FinchStationRoutesDto?

}