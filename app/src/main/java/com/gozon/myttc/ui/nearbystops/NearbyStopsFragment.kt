package com.gozon.myttc.ui.nearbystops

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gozon.myttc.R
import com.gozon.myttc.databinding.FragmentNearbyStopsBinding
import com.gozon.myttc.dto.FinchStationRoutesDto
import com.gozon.myttc.dto.Routes
import com.gozon.myttc.dto.StopTimes
import com.gozon.myttc.dto.Stops
import com.gozon.myttc.ui.listdialog.ListDialog
import com.gozon.myttc.ui.listdialog.ListType
import com.gozon.myttc.ui.listdialog.RoutesListType
import com.gozon.myttc.ui.listdialog.StopTimesListType
import com.gozon.myttc.ui.main.MainInterface
import com.gozon.myttc.utils.dialog.EmptyDialog
import com.gozon.myttc.utils.dialog.EmptyResources

class NearbyStopsFragment : Fragment(R.layout.fragment_nearby_stops),
    NearbyStopsAdapter.Parent, ListDialog.Parent {

    private var fragmentNearbyStopsFragment: FragmentNearbyStopsBinding? = null

    private lateinit var binding: FragmentNearbyStopsBinding

    private lateinit var adapter: NearbyStopsAdapter

    private val mainActivity by lazy { activity as MainInterface }

    private var finchStationRoutes: FinchStationRoutesDto? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNearbyStopsBinding.bind(view)
        fragmentNearbyStopsFragment = binding
        finchStationRoutes = mainActivity.getFinchStationRoutes()

        if (finchStationRoutes == null) {
            //TODO show no routes view
        } else {
            initializeAdapter()
        }
    }

    private fun initializeAdapter() {
        val layoutManager = LinearLayoutManager(requireContext())
        adapter = NearbyStopsAdapter(finchStationRoutes!!.stops!!, this)
        binding.stopsRecyclerView.layoutManager = layoutManager
        binding.stopsRecyclerView.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        fragmentNearbyStopsFragment = null
    }

    override fun onItemClick(name: String, data: Stops) {

        if (data.routes!!.isEmpty()) {
            showEmptyErrorDialog(EmptyResources.EMPTY_ROUTES)
        } else {
            ListDialog(name, RoutesListType(data.routes)).show(childFragmentManager, null)
        }
    }

    override fun onListItemSelected(any: Any, type: ListType) {

        when (any) {
            is Routes -> {

                if (any.stop_times!!.isEmpty()) {

                } else ListDialog(any.name!!, StopTimesListType(any.stop_times)).show(
                    childFragmentManager,
                    null
                )


            }
            is StopTimes -> {

            }
        }

    }

    override fun onListItemDismissed(type: ListType) {
    }


    private fun showEmptyErrorDialog(enum: EmptyResources) {
        EmptyDialog(enum).show(childFragmentManager, null)
    }

}