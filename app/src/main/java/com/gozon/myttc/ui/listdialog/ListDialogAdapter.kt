package com.gozon.myttc.ui.listdialog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gozon.myttc.R
import com.gozon.myttc.databinding.ListitemListDialogBinding
import com.gozon.myttc.dto.Routes
import com.gozon.myttc.dto.StopTimes
import com.gozon.myttc.utils.extensions.addTimePeriod
import com.gozon.myttc.utils.extensions.toReadableTime

class ListDialogAdapter(
    private val list: List<Any>,
    private val showArrow: Boolean,
    private val onListItemClick: (Any) -> Unit
) :
    RecyclerView.Adapter<ListDialogAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListitemListDialogBinding.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.listitem_list_dialog, parent, false)
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return when (val element = list[position]) {
            is Routes -> {
                holder.bindRoutes(element)
            }
            is StopTimes -> {
                holder.bindStopTimes(element)
            }
            else -> throw IllegalArgumentException("Invalid type for the variable element")
        }
    }

    override fun getItemCount(): Int = list.size


    inner class ViewHolder(private val binding: ListitemListDialogBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindRoutes(routes: Routes) {
            binding.listItemRoutesListLayout.visibility = View.VISIBLE
            binding.listItemStopTimesListLayout.visibility = View.GONE

            binding.routeName.text = routes.name
            binding.root.setOnClickListener {
                onListItemClick(routes)
            }
        }

        fun bindStopTimes(stopTimes: StopTimes) {
            binding.listItemRoutesListLayout.visibility = View.GONE
            binding.listItemStopTimesListLayout.visibility = View.VISIBLE

            binding.serviceId.text = stopTimes.service_id.toString()
            binding.shape.text = stopTimes.shape
            binding.departureTimestamp.text =
                stopTimes.departure_timestamp?.toReadableTime("MMMM d, yyyy")
            binding.departureTime.text = stopTimes.departure_time?.addTimePeriod()

            binding.root.setOnClickListener {
                onListItemClick(stopTimes)
            }
        }
    }

}