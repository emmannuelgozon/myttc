package com.gozon.myttc.ui.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commitNow
import androidx.lifecycle.ViewModelProviders
import com.gozon.myttc.R
import com.gozon.myttc.databinding.ActivityMainBinding
import com.gozon.myttc.dto.FinchStationRoutesDto
import com.gozon.myttc.mvvm.factory.ViewModelFactory
import com.gozon.myttc.repository.api.ApiServiceImpl
import com.gozon.myttc.viewmodel.finchstation.FinchStationViewModel
import com.gozon.myttc.viewmodel.finchstation.NetworkError
import com.gozon.myttc.viewmodel.finchstation.Success
import io.reactivex.android.schedulers.AndroidSchedulers

class MainActivity : AppCompatActivity(), MainInterface {

    private lateinit var viewModel: FinchStationViewModel
    private lateinit var binding: ActivityMainBinding

    private var finchStationRoutes: FinchStationRoutesDto? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiServiceImpl(), AndroidSchedulers.mainThread())
        )
            .get(FinchStationViewModel::class.java)

        handleLoader()
        getJson()

    }

    private fun handleLoader() {
        viewModel.loader.observe(this) { isShow ->
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    binding.loader.visibility =
                        if (isShow) View.VISIBLE
                        else View.GONE
                }, 100
            )
        }
    }


    private fun getJson() {
        viewModel.getDataFromServer()
        viewModel.finchStationData.observe(this) {
            when (it) {
                is Success -> {
                    finchStationRoutes = it.response
                    supportFragmentManager.commitNow {
                        replace(R.id.activityMainContainer, MainFragment(Command.SHOW_NEARBY_STOPS))
                    }
                }

                is NetworkError -> {
                    //TODO: create network error dialog
                }
                is UnknownError -> {
                    //TODO: create unknown error dialog
                }

            }
        }
    }

    override fun getFinchStationRoutes(): FinchStationRoutesDto? = finchStationRoutes

}