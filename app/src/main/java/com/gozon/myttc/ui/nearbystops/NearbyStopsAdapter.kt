package com.gozon.myttc.ui.nearbystops

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gozon.myttc.R
import com.gozon.myttc.databinding.ListitemStopListBinding
import com.gozon.myttc.dto.Stops

class NearbyStopsAdapter(
    private val data: List<Stops>,
    private val parent: Parent
) :
    RecyclerView.Adapter<NearbyStopsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ListitemStopListBinding.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.listitem_stop_list, parent, false)
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val element = data[position]
        holder.bind(element) {
            parent.onItemClick(element.name!!, element)
        }
    }

    inner class ViewHolder(private val binding: ListitemStopListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(stops: Stops, onClick: () -> Unit) {
            binding.listItemStopName.text = stops.name
            binding.root.setOnClickListener { onClick() }
        }
    }

    interface Parent {
        fun onItemClick(name: String, data: Stops)
    }

}