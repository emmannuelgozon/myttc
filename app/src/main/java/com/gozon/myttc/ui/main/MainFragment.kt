package com.gozon.myttc.ui.main

import android.graphics.Typeface.BOLD
import android.graphics.Typeface.NORMAL
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.commitNow
import com.gozon.myttc.R
import com.gozon.myttc.databinding.FragmentMainBinding
import com.gozon.myttc.ui.favorites.FavoritesFragment
import com.gozon.myttc.ui.nearbystops.NearbyStopsFragment
import com.gozon.myttc.ui.routes.RoutesFragment
import com.gozon.myttc.ui.stopnumber.StopNumberFragment

private const val TAG_FAVORITES = "favorites"
private const val TAG_ROUTES = "routes"
private const val TAG_NEARBY_STOPS = "nearbystops"
private const val TAG_STOP_NUMBERS = "stopnumbers"

class MainFragment() : Fragment(R.layout.fragment_main) {

    private var fragmentMainBinding: FragmentMainBinding? = null
    private lateinit var binding: FragmentMainBinding

    constructor(command: Command) : this() {
        this.command = command
    }

    private var command: Command? = null


    private lateinit var favoritesFragment: FavoritesFragment
    private lateinit var routesFragment: RoutesFragment
    private lateinit var nearbyNearbyStopsFragmentFragment: NearbyStopsFragment
    private lateinit var stopNumberFragment: StopNumberFragment

    private val fragmentList: Array<Fragment>
        get() =
            arrayOf(
                favoritesFragment,
                routesFragment,
                nearbyNearbyStopsFragmentFragment,
                stopNumberFragment
            )


    private var pageIndex = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        favoritesFragment = FavoritesFragment()
        routesFragment = RoutesFragment()
        nearbyNearbyStopsFragmentFragment = NearbyStopsFragment()
        stopNumberFragment = StopNumberFragment()

        pageIndex = when (command?.name) {
            Command.SHOW_FAVORITES.name -> 0
            Command.SHOW_ROUTES.name -> 1
            Command.SHOW_NEARBY_STOPS.name -> 2
            Command.SHOW_STOP_NUMBERS.name -> 3
            else -> throw java.lang.IllegalArgumentException("Invalid command value")
        }

        childFragmentManager.commitNow {
            add(R.id.fragmentMainContainer, favoritesFragment, TAG_FAVORITES)
            add(R.id.fragmentMainContainer, routesFragment, TAG_ROUTES)
            add(R.id.fragmentMainContainer, nearbyNearbyStopsFragmentFragment, TAG_NEARBY_STOPS)
            add(R.id.fragmentMainContainer, stopNumberFragment, TAG_STOP_NUMBERS)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentMainBinding.bind(view)
        fragmentMainBinding = binding

        when (command?.name) {
            Command.SHOW_FAVORITES.name -> binding.fragmentMainNavigationFavorites.selected()
            Command.SHOW_ROUTES.name -> binding.fragmentMainNavigationRoutes.selected()
            Command.SHOW_NEARBY_STOPS.name -> binding.fragmentMainNavigationNearbyStops.selected()
            Command.SHOW_STOP_NUMBERS.name -> binding.fragmentMainNavigationStopNumber.selected()
            else -> throw java.lang.IllegalArgumentException("Invalid command value")
        }

        binding.fragmentMainNavigationFavorites.setOnClickListener {
            setFragment(favoritesFragment)
            highlightText(binding.fragmentMainNavigationFavorites)
        }

        binding.fragmentMainNavigationRoutes.setOnClickListener {
            setFragment(routesFragment)
            highlightText(binding.fragmentMainNavigationRoutes)
        }

        binding.fragmentMainNavigationNearbyStops.setOnClickListener {
            setFragment(nearbyNearbyStopsFragmentFragment)
            highlightText(binding.fragmentMainNavigationNearbyStops)
        }

        binding.fragmentMainNavigationStopNumber.setOnClickListener {
            setFragment(stopNumberFragment)
            highlightText(binding.fragmentMainNavigationStopNumber)
        }

        if (pageIndex > -1) {
            setFragment(
                when (pageIndex) {
                    0 -> favoritesFragment
                    1 -> routesFragment
                    2 -> nearbyNearbyStopsFragmentFragment
                    3 -> stopNumberFragment
                    else -> throw IllegalArgumentException("invalid page index")
                }
            )
        }
    }

    private fun setFragment(fragmentToShow: Fragment) {

        childFragmentManager.commit {
            for (fragment in fragmentList) {
                if (fragmentToShow == fragment) {
                    attach(fragment)
                } else {
                    detach(fragment)
                }
            }
        }
    }


    private fun highlightText(button: TextView) {
        for (b in listOf(
            binding.fragmentMainNavigationFavorites,
            binding.fragmentMainNavigationRoutes,
            binding.fragmentMainNavigationNearbyStops,
            binding.fragmentMainNavigationStopNumber
        )) {

            if (button == b) {
                b.selected()
            } else {
                b.removeSelected()
            }
        }
    }

    private fun TextView.selected() {
        isSelected = true
        setTypeface(null, BOLD)
    }

    private fun TextView.removeSelected() {
        isSelected = false
        setTypeface(null, NORMAL)
    }


    override fun onDestroy() {
        super.onDestroy()
        fragmentMainBinding = null
    }
}

enum class Command {
    SHOW_FAVORITES,
    SHOW_ROUTES,
    SHOW_NEARBY_STOPS,
    SHOW_STOP_NUMBERS
}

