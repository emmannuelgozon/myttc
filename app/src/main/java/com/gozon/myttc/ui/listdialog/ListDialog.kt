package com.gozon.myttc.ui.listdialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gozon.myttc.R
import com.gozon.myttc.databinding.DialogListBinding
import com.gozon.myttc.dto.Routes
import com.gozon.myttc.dto.StopTimes
import java.io.Serializable

class ListDialog() : DialogFragment(R.layout.dialog_list) {

    constructor(title: String, dialogType: ListType) : this() {
        this.dialogType = dialogType
        this.dialogTitle = title
    }

    private val parent by lazy { parentFragment as Parent }

    private var routesRoutesListFragment: DialogListBinding? = null
    private lateinit var binding: DialogListBinding

    private var dialogTitle : String? = null
    private var dialogType : ListType? = null

    private lateinit var adapter: ListDialogAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dialog!!.requestWindowFeature(STYLE_NO_TITLE)
        return inflater.inflate(R.layout.dialog_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = DialogListBinding.bind(view)
        routesRoutesListFragment = binding

        val window = dialog!!.window!!
        window.setBackgroundDrawable(InsetDrawable(ColorDrawable(Color.WHITE), 0))
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        initView()
        initList()
    }

    private fun initView() {
        binding.dialogListImageViewArrowBack.setOnClickListener {
            dismiss()
        }
        binding.dialogListToolbarTitle.text = dialogTitle
    }

    private fun initList() {
        val layoutManager = LinearLayoutManager(requireContext())

        val showArrow = when (dialogType) {
            is RoutesListType -> true
            is StopTimesListType -> false
            else -> false
        }

        val list = when (dialogType) {
            is RoutesListType -> {
                (dialogType as RoutesListType).routesList
            }
            is StopTimesListType -> {
                (dialogType as StopTimesListType).stopTimesList
            }
            else -> throw IllegalArgumentException("Invalid type for the variable $dialogType")
        }

        adapter = ListDialogAdapter(list, showArrow) { any ->
            parent.onListItemSelected(any, dialogType!!)
        }
        binding.dialogListRecyclerView.layoutManager = layoutManager
        binding.dialogListRecyclerView.adapter = adapter
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        parent.onListItemDismissed(dialogType!!)
    }

    interface Parent {
        fun onListItemSelected(any: Any, type: ListType)
        fun onListItemDismissed(type: ListType)
    }

}

interface ListType : Serializable

class RoutesListType(val routesList: List<Routes>) : ListType
class StopTimesListType(val stopTimesList: List<StopTimes>) : ListType
