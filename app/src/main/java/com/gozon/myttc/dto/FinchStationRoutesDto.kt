package com.gozon.myttc.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FinchStationRoutesDto(
    val uri: String?,
    val name: String?,
    val time: Long?,
    val stops: List<Stops>?
) : Parcelable

@Parcelize
data class Stops(
    val uri: String?,
    val name: String?,
    val agency: String?,
    val routes: List<Routes>?
) : Parcelable

@Parcelize
data class Routes(
    val uri: String?,
    val name: String?,
    val stop_times: List<StopTimes>?,
    val route_group_id: String?
) : Parcelable

@Parcelize
data class StopTimes(
    val shape: String?,
    val service_id: Int?,
    val departure_timestamp: Long?,
    val departure_time: String?
) : Parcelable